package main.java.com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public abstract class FiguraGeometrica {
    
    public abstract double area();
    public abstract double perimetro();
    
    @Override
    public String toString ()
    {
        return "área: "+ area()+ "\n"+ "Perímetro: "+ perimetro() ;
    }
            
    }
            
