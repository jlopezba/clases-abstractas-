package main.java.com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Rectangulo extends FiguraGeometrica{
    private double base;
    private double altura;

    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public double area()
    {
    return base*altura;
    }
    
    @Override
    public double perimetro()
    {
    return (base*2)+(altura+2);
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}
